﻿#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>

using namespace std;

//функция очистки строки
string clearString(string str, const set<char>& symbols) {
	size_t startIdx = 0, endIdx = str.length() - 1;			//стартовый и конечный индекс в слове, сначала укаывают на конец и начало
	bool startFlag = true, endFlag = true;					//флаги начала слова без спец симфолов

	//если не дошли до слова, то 
	while (startFlag || endFlag) {

		//смотрим конечный символ
		if (symbols.count(str[endIdx]) == 0) {
			endFlag = false;								//если он есть в сете спец символов, то заканчиваем обработку конца слова
		}
		else {
			endIdx -= 1;									//двигаемся дальше
		}

		//начальный символ
		if (symbols.count(str[startIdx]) == 0) {
			startFlag = false;								//если он есть в сете спец символов, то заканчиваем обработку начала слова
		}
		else {
			startIdx += 1;									//двигаемся дальше
		}
	}

	//берем подстроку без спецсимволов
	str = str.substr(startIdx, endIdx - startIdx + 1);

	//трансформируем слово в малый регистр
	transform(begin(str), end(str), begin(str), ::tolower);
	return str;
}

//функция подсчета слов в строке
map<string, size_t> wordCount(const string& str) {
	//выбрана структура map для хранения кол-ва уникальных слов 
	//ключ - слово, значение - количество
	map<string, size_t> words;			

	//переписываем строку в поток
	stringstream ss(str);

	//текущее обрабатываемое слово
	string currentWord;

	//пока в потоке есть, что читать - читаем
	while (ss >> currentWord) {
		//очищаем слово от спец символов с концов
		currentWord = clearString(currentWord, { '.', ',', '!', '?', '\"', '\'', '(', ')', '{', '}', '[', ']' , '\'', '\"', ';', ':', '-'});

		//ведем подсчет
		if (words.count(currentWord) > 0) {
			words[currentWord] += 1;
		}
		else {
			words[currentWord] = 1;
		}
	}
	return words;
}

//функция печати уникальных слов и их количества
void printMap(const map<string, size_t>& words) {
	for (const auto& [word, count] : words) {
		cout << word << " - " << count << endl;
	}
}

int main(int argc, char* argv[])
{
	try {

		//чтение аргументов программы
		string filename;												//имя входного файла
		if (argc == 2) {
			filename = argv[1];											//если аргумент один(не считая имени программы), то имя записываем имя файла
		}
		else {
			throw invalid_argument("Invalid program arguments");		//в других случаях выбрасываем исключение
		}

		//чтение файла
		ifstream inputFile(filename);									//открываем файл
		if (inputFile.is_open()) {										//проверяем, открыт ли файл
			stringstream ss;
			ss << inputFile.rdbuf();									//если открыт, то считываем весь файл
			inputFile.close();											//закрываем файл
			map<string, size_t> words = wordCount(ss.str());			//вызываем подсчет слов
			printMap(words);											//печать
		}
		else {
			throw runtime_error("Can not open file: " + filename);		//если файл не удалось открыть, то выбрасываем исключение
		}
	}
	catch (exception & e) {
		cout << e.what() << endl;										//печатаем исключение
	}
	return 0;
}
